env/ready: requirements-dev.txt env/bin/pip
	env/bin/pip install -r requirements-dev.txt -e .
	touch env/ready

requirements-dev.txt: requirements.in requirements-dev.in env/bin/pip-compile
	env/bin/pip-compile --no-index requirements.in requirements-dev.in -o requirements-dev.txt

env/bin/pip-compile: env/bin/pip
	env/bin/pip install pip-tools

env/bin/pip:
	python -m venv env

.PHONY: start
start: env/ready
	env/bin/honcho start

.PHONY: stop
stop: env/ready
	env/bin/honcho stop

.PHONY: webpack-build
webpack-build: env/ready
	node_modules/webpack/bin/webpack.js --config ./webpack.config.js --mode production

.PHONY: migrate
migrate: env/ready
	env/bin/manage.py migrate

.PHONY: clean-source-map-urls
clean-source-map-urls:
	# To avoid errors like this: TypeError: interact.js.map is not a valid URL.
	find node_modules -type f -iname '*.js' -exec sed -i '\|^//# sourceMappingURL.*\.map$$|d' {} \;
