Handlers
========

.. default-role:: literal

.. role:: term


`merge(*args: Union[dict, Expression]) -> DictHandler`
  Merge list of dicts and :term:`expressions <expression>` returning a merged dicts.

`chain(*args: Expression) -> Handler`
  Chain all given :term:`expressions <expression>`, where each
  :term:`expression` pushes result to term:`this`.  `chain` itself returns
  wathever last :term:`expression` returns.

`define(**kwargs) -> None`
  Define new context variables, these variables can be accessed anywhere in
  :term:`expression` using `this.c` expression. Example:

  .. code-block:: python

    assert this.define(a=42).c.a.execute(1) == 42

  Context also can be passed value `execute`:

  .. code-block:: python

    assert this.c.a.execute(1, a=42) == 42

`when(condition: Expression, expression: Expression) -> Handler`
  Execute expression only if given condition returns true. Example:

  .. code-block:: python

    expression = merge(
        {
            'type': this.type.oneof('foo', 'bar'),
            'value': 0,
        },
        when(this.type == 'foo', {
            'value': 1,
        }),
        when(this.type == 'bar', {
            'value': 2,
        }),
    )
    assert expression.execute({'type': 'bar'}) == {
        'type': 'bar',
        'value': 2,
    }

`check(condition, error_message, error_message_kwargs) -> None`
  If `condition` returns `False`, `ValidationError` is raised. Example:

  .. code-block:: python

    expression = using(this, {
        'foo':
            this.
            check(this < 10, "number should be less than 10, got {number}", {'number': this}).
            check(this % 2 != 0, "odd number expected"),

        'bar':
            this.check(this.length() < 10, "too long"),
    })

    try:
        expression.execute({
            'foo': 42,
            'bar': 'baz',
        })
    except ValidationError as e:
        assert e.value == {
            'foo': 42,
            'bar': 'baz',
        }
        assert e.errors == {
            'foo': [
                "number should be less than 10, got 42",
                "odd number expected",
            ],
        }

  If any of checks fail, `execute` raises `ValidationError` where you can get
  value that was executed without errors and list of errors.



.. glossary::

  expression
    Expression is an `Expression` class instance representing execution
    instructions to be executed later.

    For example:

    .. code-block:: python

      assert this.upper().split().execute('a b') == ['A', 'B']

    Here expression `this.upper().split()` is executed with `'a b'` value.
    Value was transformed into upper case letters, and then it was split into
    list of letters.

  this
    `this` is an expression always representing current value.
