import re
import operator

from databot import task, this, loop, do, put, merge


def merge_intervals(intervals):
    if intervals:
        intervals = iter(sorted(intervals, key=operator.itemgetter(0)))
        low, high = next(intervals)
        for a, b in intervals:
            if a <= high:
                high = max(high, b)
            else:
                yield low, high
                low, high = a, b
        yield low, high


def parse_title(title):
    parapija, tail = title.split(' RKB ', 1)
    years = []
    for start, _, _, end in re.findall(r'(\d{4})((--|-|–)(\d{4}))? m\.', tail):
        start = int(start)
        end = int(end) if end else start
        years.append((start, end))
    if years:
        years = list(merge_intervals(years))
        start, end = zip(*years)
        start = min(start)
        end = max(end)
        total = sum(b - a for a, b in years)
    else:
        start = end = 0
    return {
        'parapija': parapija,
        'pradžia': start,
        'pabaiga': end,
        'trukmė': total,
    }


pipeline = {
    'tasks': [
        task('paieška').
            monthly().selenium().do(
                this.goto('http://www.epaveldas.lt/patikslintoji-paieska'),
                this.css('.inputParam').one().send_keys('RKB metrikų knyga'),
                this.css('.btn2Parts').one().click(),
                this.xpath('//select[contains(@id, "SlctPageSize")]/option[@value="50"]').one().click(),
                this.wait(this.css('.wInfo .searchResultDescription a').length() == 50),
                loop(this.xpath('//img[@title="Sekantis psl."]/..').one().style.contains('cursor:default') == False, do(
                    this.css('.wInfo .searchResultDescription a').each((this.href, this.text)).cast(list).put(result=this),
                    this.xpath('//img[@title="Sekantis psl."]/..').one().click().
                    this.wait(this.c.result.first().is_stale()),
                    this.c.result,
                ))
            ).
            pipe('paieškos-nuorodos').write(),

        task('siuntimas').
            pipe('paieškos-nuorodos').each(this.fetch()).
            pipe('paieškos-puslapiai').write(),

        task('ištraukimas').
            pipe('paieškos-puslapiai').each(do(
                put(html=this.html()),
                merge(
                    this.c.html.css('.entryTable tr').each((
                        this.css('th').one().text(),
                        this.css('td').one().text().strip(),
                    )).cast(dict),
                    this.c.html.css('.authorTitle').one().text().apply(parse_title),
                    {
                        'url': this.url,
                        'antraštė': this.c.html.css('.authorTitle').one().text(),
                    },
                )
            )).
            pipe('knygos-duomenys').write(),
    ],
}  # noqa: E131
