from databot import task, this, that, chain, merge, fetch, pipe


pipeline = {
    'pipes': [
        pipe('nuorodos', key=this.url),
    ],
    'tasks': [
        task('nuorodos').
            daily().
            sql('ivpk.irs.rinkmenos').
            select('''
                SELECT
                    t_rinkmena.ID,
                    t_rinkmena.TINKLAPIS
                FROM t_rinkmena
                WHERE STATUSAS = 'U'
            ''').
            where(pipe('nuorodos').exists(this['t_rinkmena.TINKLAPIS']) == False).
            each(merge(
                fetch(this['t_rinkmena.TINKLAPIS']),
                {
                    'id': this['t_rinkmena.ID'],
                    'url': this['t_rinkmena.TINKLAPIS'],
                    'depth': 0,
                },
            )).
            pipe('nuorodos').write(),

        task('resursai').
            pipe('nuorodos').
            where(this.headers['content-type'].contains('text/html')).
            each(
                this.
                html().
                xpath('//@href').
                where(pipe('nuorodos').where(this.link == that).exists() == False).
                each(...)
            ).
            pipe('resursai').write(),

        task('rinkmenos').
            daily().
            sql('ivpk.irs.rinkmenos').
            select('''
                SELECT
                    t_rinkmena.ID,
                    t_rinkmena.PAVADINIMAS,
                    t_rinkmena.SANTRAUKA,
                    t_rinkmena.TINKLAPIS,
                    t_user.FIRST_NAME,
                    t_user.LAST_NAME,
                    t_rinkmena.K_EMAIL,
                    t_istaiga.PAVADINIMAS,
                    t_rinkmena.R_ZODZIAI
                FROM t_rinkmena
                LEFT JOIN t_user ON (t_rinkmena.USER_ID = t_user.ID)
                LEFT JOIN t_istaiga ON (t_rinkmena.istaiga_id = t_istaiga.ID)
                WHERE STATUSAS = 'U'
            ''').
            each({
                'id': this['t_rinkmena.ID'],
                'name': this['t_rinkmena.PAVADINIMAS'].slugify(),
                'title': this['t_rinkmena.PAVADINIMAS'],
                'notes': this['t_rinkmena.SANTRAUKA'],
                'url': this['t_rinkmena.TINKLAPIS'],
                'maintainer': chain(this['t_user.FIRST_NAME'], this['t_user.LAST_NAME']).join(),
                'maintainer_email': this['t_rinkmena.K_EMAIL'],
                'owner_org': this['t_istaiga.PAVADINIMAS'],
                'state': 'active',
                'resources': pipe('nuorodos').each({
                    'url': this['url'],
                    'name': this['name'],
                    'format': this['type'],
                }),
                'tags': (
                    this['t_rinkmena.R_ZODZIAI'].lower().
                    replace(';', ',').split(',').each(this.strip()).
                    where(this.length() > 2, this.length() < 100).
                    each({'name': this.strip()})
                ),
                'groups': this.sql('ivpk.irs.rinkmenos', '''
                    SELECT DISTINCT
                        t_kategorija.PAVADINIMAS,
                        t_kategorija.ID
                    FROM t_kategorija
                    INNER JOIN t_kategorija_rinkmena ON (t_kategorija.ID = t_kategorija_rinkmena.KATEGORIJA_ID)
                    WHERE t_kategorija_rinkmena.RINKMENA_ID = $1
                    ''',
                    this['t_rinkmena.ID']).
                    select(chain(this['PAVADINIMAS'], this['ID']).join().slugify()).
                    select({'name': this}),
                'extras': [
                        {'key': 'Šaltinis', 'value': 'IVPK IRS'},
                        {'key': 'Šaltinio ID', 'value': this['ID']},
                        {'key': 'Kodas', 'value': this['KODAS']},
                    ],
            }).
            pipe('1990/sąrašo-puslapis').write(),
    ]
}  # noqa: E131
