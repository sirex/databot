from itertools import groupby
from operator import itemgetter

from databot import pipe, task, this, chain, fetch


MONTHS = {
    'sausio': 1,
    'vasario': 2,
    'kovo': 3,
    'balandžio': 4,
    'gegužės': 5,
    'birželio': 6,
    'liepos': 7,
    'rugpjūčio': 8,
    'rugsėjo': 9,
    'spalio': 10,
    'lapkričio': 11,
    'gruodžio': 12,
}


def date(value, p_asm_id=None):
    spl = value.replace('-', ' ').split()
    if len(spl) == 3:
        return '-'.join(spl)
    elif len(spl) == 4:
        # 1945 balandžio 10 d.
        return '-'.join([spl[0], str(MONTHS[spl[1].lower()]).zfill(2), spl[2].zfill(2)])
    elif len(spl) == 5:
        # 1945 m. balandžio 10 d.
        return '-'.join([spl[0], str(MONTHS[spl[2].lower()]).zfill(2), spl[3].zfill(2)])
    elif len(spl) == 6:
        # 1945 m. balandžio mėn 10 d.
        return '-'.join([spl[0], str(MONTHS[spl[2].lower()]).zfill(2), spl[4].zfill(2)])
    else:
        return value


def case_split(value):
    words = [(w.isupper(), w) for w in value.split()]
    return [' '.join([x for _, x in g]) for k, g in groupby(words, key=itemgetter(0))]


pipeline = {
    'pipes': [
        pipe('1990/sąrašo-puslapis', key='url'),
    ],
    'tasks': [
        task('1990/sąrašo-puslapis').monthly().
            fetch('http://www3.lrs.lt/pls/inter/w5_lrs.seimo_nariu_sarasas?p_kade_id=1').
            pipe('1990/sąrašo-puslapis').write(),

        task('1990/sąrašo-duoemnis').
            pipe('1990/sąrašo-puslapis').each(
                this.html().xpath('//td[h1/h2/text()="Seimo narių sąrašas"]/ol/li/a').each(
                    this.put(name=this.text().apply(case_split)).
                    get({
                        'nuoroda': this.xpath('@href').get().pk(),
                        'vardas': this.c.name[0],
                        'pavardė': this.c.name[1],
                    })
                )
            ).
            pipe('1990/sąrašo-duomenys').write(),

        task('1990/seimo-nario-puslapis').
            pipe('1990/sąrašo-duomenys').each(fetch(this.nuoroda)).
            pipe('1990/seimo-nario-puslapis').write(),

        task('1990/seimo-nario-duomenys').
            pipe('1990/seimo-nario-puslapis').each(
                this.
                put(p_asm_id=this.url.urlparse().query['p_asm_id']).
                html().
                put(pareigos=this.css('#SN_pareigos')).
                put(vardas=this.c.pareigos.xpath('.//h3[1]/br/following-sibling::text()[1]').one().apply(case_split)).
                get({
                    'p_asm_id': this.c.p_asm_id,
                    'seimas': 'V',
                    'kadencija': '1990–1992',
                    'vardas': this.c.vardas[0],
                    'pavardė': this.c.vardas[1],
                    'mandatas': {
                        'nuo': this.c.pareigos.xpath('//p/text()[. = " nuo "]/following-sibling::b[1]/text()').get().replace(' ', '-'),
                        'iki': this.c.pareigos.xpath('//p/text()[. = " iki "]/following-sibling::b[1]/text()').get().replace(' ', '-'),
                    },
                    'išrinko': chain([
                        this.c.pareigos.xpath('//p/text()[. = "Išrinktas  "]/following-sibling::b[1]/text()').get().strip(),
                        this.c.pareigos.xpath('//p/text()[. = "Išrinktas  "]/following-sibling::text()[1]').get().strip(),
                        this.c.pareigos.xpath('//p/text()[. = "Išrinkta  "]/following-sibling::b[1]/text()').get().strip(),
                        this.c.pareigos.xpath('//p/text()[. = "Išrinkta  "]/following-sibling::text()[1]').get().strip(),
                    ]).join(' '),
                    'iškėlė': this.c.pareigos.xpath('//p/text()[. = "iškėlė "]/following-sibling::b[1]').optional().text(),
                    'nuotrauka': this.c.pareigos.css('img@src'),
                    'biografija': this.xpath('//b[text() = "Biografija"]/ancestor::table[1]').text().replace('\xad', ''),
                    'gimė':
                        this.xpath('//b[text() = "Biografija"]/ancestor::table[1]').text().replace('\xad', '').
                        re(r'Gim[eė] (\d{4} \d{2} \d{2}|\d{4} m\. \w+ \d+ d\.)').apply(date),
                })
            ).
            pipe('1990/seimo-nario-duomenys').write(),
    ]
}  # noqa: E131
