import logging
import inspect
import traceback
import textwrap
from typing import Dict, List
from contextlib import contextmanager
from collections import namedtuple

import pprintpp

logger = logging.getLogger(__name__)


Error = namedtuple('Error', ('message', 'kwargs', 'traceback', 'exception'))


class ExecutionError(Exception):

    def __init__(self, result, errors):
        self.result = result
        self.errors = errors

    def __str__(self):
        lines = []
        for error in self.errors:
            lines.append('Expression:')
            lines.append(textwrap.indent(str(error.traceback), '  '))
            if error.exception:
                lines.append('Traceback:')
                lines.append(''.join(traceback.format_tb(error.exception.__traceback__)).rstrip())
            lines.append('Error: ' + error.message.format(**error.kwargs))
        return '\n'.join(lines)


class Op:

    def __init__(self, name, args=(), kwargs=None):
        self.name = name
        self.args = args
        self.kwargs = kwargs or {}


def argrepr(value, nested=False):
    if isinstance(value, list):
        value = [argrepr(x, nested=True) for x in value]
    elif isinstance(value, tuple):
        value = tuple([argrepr(x, nested=True) for x in value])
    elif isinstance(value, dict):
        value = {k: argrepr(v, nested=True) for k, v in sorted(value.items())}
    elif inspect.isclass(value):
        value = value.__name__

    if nested:
        return value
    else:
        return pprintpp.pformat(value)


class Expression:

    def __init__(self, _name_, _stack_=(), *args, **kwargs):
        self._stack = _stack_ + (Op(_name_, args, kwargs),)

    def __str__(self):
        result = ''
        for i, op in enumerate(stack(self)):
            if i > 0:
                result += '.'

            if op.name == '__call__':
                args = ', '.join(
                    [argrepr(x) for x in op.args] +  # noqa: W504
                    ['%s=%s' % (k, argrepr(v)) for k, v in sorted(op.kwargs.items())]
                )
                result += '%s(%s)' % (op.name, args)

            elif op.name == '__getattr__':
                result += '.' + op.args[0]

            elif op.name == '__getitem__':
                result += '[' + str(op.args[0]) + ']'

            elif op.name == '__gt__':
                result += ' > ' + repr(op.args[0])

            elif op.name == '__lt__':
                result += ' , ' + repr(op.args[0])

            else:
                result += op.name

        return result

    __repr__ = __str__

    def __eq__(self, *args, **kwargs):
        return Expression('__eq__', self._stack, *args, **kwargs)

    def __gt__(self, *args, **kwargs):
        return Expression('__gt__', self._stack, *args, **kwargs)

    def __lt__(self, *args, **kwargs):
        return Expression('__lt__', self._stack, *args, **kwargs)

    def __pow__(self, *args, **kwargs):
        return Expression('__pow__', self._stack, *args, **kwargs)

    def __getitem__(self, *args, **kwargs):
        return Expression('__getitem__', self._stack, *args, **kwargs)

    def __getattr__(self, key):
        # Do not add protected methods to the stack.
        if key.startswith('_'):
            super().__getattr__(key)
        else:
            return Expression('__getattr__', self._stack, key)

    def __call__(self, *args, **kwargs):
        return Expression('__call__', self._stack, *args, **kwargs)


class Context:

    def __init__(self, **context):
        self.context = [context]

    @contextmanager
    def __call__(self, **context):
        self.context.append({**self.context[-1], **context})
        yield
        self.context.pop()

    def __getattr__(self, name):
        return self.context[-1][name]


def stack(expr):
    """Read Expression call stack."""

    skip = False
    for i, (op, next_op) in enumerate(zip(expr._stack, expr._stack[1:] + (None,))):
        if skip:
            skip = False
            continue

        if next_op and next_op.name == '__call__':
            # Method call.
            if op.name == '__getattr__':
                name, = op.args

            # Function call.
            elif i == 0:
                name = op.name

            else:
                raise Exception("Unknown call expression.")

            yield Op(name, next_op.args, next_op.kwargs)

            skip = True

        else:
            yield op


class Stack:

    def __init__(self, expr):
        self.stack = stack(expr)
        self.path = []

    def pop(self):
        try:
            return next(self.stack)
        except StopIteration:
            return


class StackManager:

    def __init__(self):
        self.stacks = []

    @contextmanager
    def __call__(self, expr):
        self.stacks.append(Stack(expr))
        yield
        self.stacks.pop()

    @contextmanager
    def path(self, item):
        self.stacks[-1].path.append(item)
        yield
        self.stacks[-1].path.pop()

    def pop(self):
        return self.stacks[-1].pop()


class Handler:
    PARAMS = {'executor', 'context', 'expr', 'this'}

    def __init__(self, type, func, name=None, **kwargs):
        self.type = type
        self.name = name or func.__name__
        self.func = func

        def set_kwargs(step=True, args=True, params=None):
            self.step = step
            self.args = args
            self.params = self.detect_func_params() if params is None else params

        set_kwargs(**{**getattr(func, '_annotate_kwargs', {}), **kwargs})

    def __repr__(self):
        return f'Handler({self.name}, {self.func.__name__}, {self.type.__name__})'

    def detect_func_params(self):
        params = []
        sig = inspect.signature(self.func)
        for param in sig.parameters.keys():
            if param in self.PARAMS:
                params.append(param)
            else:
                break
        return params

    def __call__(self, executor, op, expr, this):
        params = {
            'executor': executor,
            'context': executor.context,
            'expr': expr,
            'this': this,
        }
        args = tuple(params[p] for p in self.params)

        if self.args:
            args += executor.execute(op.args, this)
            kwargs = executor.execute(op.kwargs, this)

        else:
            args += op.args
            kwargs = op.kwargs

        this = self.func(*args, **kwargs)

        if self.step:
            this = executor.step(expr, this)

        return this


class Trace:

    def __init__(self, op, handler, value):
        self.op = op
        self.handler = handler
        self.value = value


class Traceback:

    def __init__(self, stack=None):
        self.stack = stack or []

    @contextmanager
    def __call__(self, op, handler, value):
        trace = Trace(op, handler, value)
        self.stack.append(trace)
        yield trace
        self.stack.pop()

    def __str__(self):
        lines = []
        for trace in self.stack:
            lines.append('%s.%s | %s' % (
                getattr(trace.handler.func, '__module__', None) or type(trace.value).__name__,
                trace.handler.func.__name__,
                argrepr(trace.value),
            ))
        return '\n'.join(lines)

    def empty(self):
        return len(self.stack) == 0

    def copy(self):
        return Traceback(list(self.stack))


def default_object_handler(op, value):
    func = getattr(value, op.name)
    return Handler(type(value), func, params=())


class Executor:

    def __init__(self, registry, **context):
        self.registry = registry
        self.context = Context(**context)
        self.stack = StackManager()
        self.errors = []
        self.traceback = Traceback()

    def error(self, message, kwargs=None, exception=None, traceback=None):
        self.errors.append(Error(
            message,
            kwargs or {},
            self.traceback.copy(),
            exception,
        ))
        return NA

    def step(self, expr, this):
        op = self.stack.pop()
        return self.execute(expr, this, op) if op else this

    def execute(self, expr, this, op=None):
        if op is None:
            op = Op('__expr__')
            value = expr
        else:
            value = this

        handler = (
            self.registry.find_handler(op.name, value) or  # noqa: W504
            default_object_handler(op, value)
        )

        with self.traceback(op, handler, value):
            try:
                this = handler(self, op, expr, this)
            except Exception as e:
                self.error(str(e), exception=e)

        if self.traceback.empty() and self.errors:
            raise ExecutionError(this, self.errors)

        return this


class Registry:
    handlers: Dict[str, List[Handler]]

    def __init__(self):
        self.handlers = {}

    def add(self, handlers):
        def _sort_key(handler):
            return len(handler.type.mro())

        for handler in handlers:
            if handler.name not in self.handlers:
                self.handlers[handler.name] = []

            self.handlers[handler.name].append(handler)

            # Sort handlers by type specifisity.
            self.handlers[handler.name].sort(key=_sort_key, reverse=True)

    def find_handler(self, name, value):
        for handler in self.handlers.get(name, []):
            if isinstance(value, handler.type):
                return handler


def annotate(**kwargs):
    """Add `_annotate_kwargs` attribute to the decorated function."""
    def decorator(func):
        func._annotate_kwargs = kwargs
        return func
    return decorator


class NotAvailable:

    def __repr__(self):
        return "NA"

    __str__ = __repr__


NA = NotAvailable()
