import re

from databot.expressions import Expression, Handler, NA, annotate


def _getattr_(context, this, key):
    if key in ('c', 'context'):
        if isinstance(this, dict) and key in this:
            return this[key]
        elif hasattr(this, key):
            return getattr(this, key)
        else:
            return context
    elif isinstance(this, dict):
        return this[key]
    else:
        return getattr(this, key)


def _eq_(this, other):
    return this == other


def _pow_(this, other, modulo=None):
    if modulo:
        return pow(this, other, modulo)
    else:
        return pow(this, other)


def this(this):
    return this


def call(func, *args, **kwargs):
    return func(*args, **kwargs)


def apply(this, func, *args, **kwargs):
    return func(this, *args, **kwargs)


def cast(this, type):
    return type(this)


def define(executor, expr, this, **kwargs):
    with executor.context(**kwargs):
        return executor.step(expr, this)


@annotate(args=False)
def when(executor, this, condition, expression):
    this = executor.execute(condition, this)
    if this:
        return executor.execute(expression, this)
    else:
        return NA


def check(executor, this, condition, message, kwargs):
    if not condition:
        executor.error(message, kwargs)
    return this


@annotate(args=False)
def each(executor, this, expression):
    if isinstance(this, list):
        return [executor.execute(expression, x) for x in this]
    elif isinstance(this, tuple):
        return tuple([executor.execute(expression, x) for x in this])
    elif isinstance(this, set):
        return {executor.execute(expression, x) for x in this}
    elif isinstance(this, dict):
        return {k: executor.execute(expression, x) for k, x in this.items()}


@annotate(step=False, args=False)
def _expression_(executor, expr, this):
    with executor.stack(expr):
        return executor.step(expr, this)


@annotate(step=False, args=False)
def _tuple_(executor, expr, this):
    result = []
    for i, value in enumerate(expr):
        with executor.stack.path(i):
            result.append(executor.execute(value, this))
    return tuple(result)


@annotate(step=False, args=False)
def _list_(executor, expr, this):
    result = []
    for i, value in enumerate(expr):
        with executor.stack.path(i):
            result.append(executor.execute(value, this))
    return result


@annotate(step=False, args=False)
def _dict_(executor, expr, this):
    result = {}
    for key, value in expr.items():
        with executor.stack.path(key):
            result[key] = executor.execute(value, this)
    return result


@annotate(step=False, args=False)
def _expr_(expr):
    return expr


def sub(this, pattern, repl, count=0, flags=0):
    return re.sub(pattern, repl, this, count=count, flags=flags)


handlers = [
    Handler(object, this),
    Handler(object, call),
    Handler(object, apply),
    Handler(object, cast),
    Handler(object, define),
    Handler(object, when),
    Handler(object, check),
    Handler(object, _getattr_, '__getattr__'),
    Handler(Expression, _expression_, '__expr__'),
    Handler(tuple, _tuple_, '__expr__'),
    Handler(list, _list_, '__expr__'),
    Handler(dict, _dict_, '__expr__'),
    Handler(object, _expr_, '__expr__'),
    Handler(list, each),
    Handler(tuple, each),
    Handler(set, each),
    Handler(dict, each),
    Handler(str, sub),
]


this = Expression('this')
that = Expression('that')
when = Expression('when')
check = Expression('check')
each = Expression('each')
sub = Expression('sub')
chain = Expression('chain')
merge = Expression('merge')
fetch = Expression('fetch')
loop = Expression('loop')
do = Expression('do')
define = Expression('define')
task = Expression('task')
put = Expression('put')
pipe = Expression('pipe')
