import argparse
import asyncio
import datetime
import uuid

import msgpack
import websockets

from databot.handlers import this, when, call, check
from databot import get_pipeline_version
from databot.expressions import ExecutionError
from databot.storage import get_storage


SECRET_KEY = 'secret'
JWT_ALGORITHM = 'HS256'


schema = this.define(message=this).merge(
    {
        'type': this.type.oneof('registration', 'task', 'data', 'result'),
    },
    when(this.type == 'registration', {
        'token':
            this.token.jwt(SECRET_KEY, algorithm=JWT_ALGORITHM).
            storage().tokens.get(this.id),
    }),
    when(this.type != 'registration', (
        check(this.token.exp > call(datetime.datetime.utcnow), "access token has expired"),
    )),
    when(this.type in ('data', 'result'), {
        'worker':
            this.c.workers[this.worker].null().
            check(this.message.pipeline.name == this.pipeline.name, "expected pipeline: {expected}, got: {received}", {
                'expected': this.message.pipeline,
                'received': this.pipeline,
            }).
            check(this.message.row == this.row, "expected row: {expected}, got: {received}", {
                'expected': this.message.row,
                'received': this.row,
            }),
        'pipeline': {
            'name': this.pipeline.name,
            'version': this.pipeline.version,
        },
    }),
    when(this.type == 'data', {
        'token': this.token.dtype(str),
        'offset': this.offset.dtype(int),
    }),
)  # noqa: E131


class Master:

    def __init__(self, storage, pipeline):
        self.storage = storage
        self.pipeline = pipeline
        self.workers = {}
        self.requests = {}

    def registration(self, message):
        worker = str(uuid.uuid4())
        self.workers[worker] = {
            'token': message['token']['id'],
            'busy': False,
        }
        return {
            'type': 'registration',
            'worker': worker,
            'status': 'ok',
        }

    def task(self, message):
        pipeline = (
            self.tasks.
            define(
                pipeline=this.pipeline.execute(message),
                task=this.task.execute(message),
            ).
            where(
                when(this.c.pipeline, this.pipeline == this.c.pipeline),
                when(this.c.task, this.task == this.c.task),
            ).
            join('jobs', self.storage.jobs.groupby(this.pipeline, this.task), (
                this.pipeline == this.c.pipeline,
                this.task == this.c.task,
            )).
            shuffle().sort(this.joins.jobs.count().desc()).first()
        )
        return {
            'type': 'pipeline',
            'name': pipeline['name'],
            'script': pipeline['script'],
            'version': pipeline['version'],
        }

    def read(self, message):
        version = get_pipeline_version(message['pipeline']['name'])
        if message['pipeline']['version'] != version:
            # Request pipeline update if pipeline has changed.
            return {
                'type': 'pipeline-outdated',
                'name': message['pipeline']['name'],
                'version': version,
            }
        else:
            data = (
                self.storage(message['pipeline']['name']).
                join('job', this.storage.jobs, (
                    this.pipeline == this.c.pipeline,
                    this.task == this.c.task,
                    this.row == this.c.pk,
                )).
                where(this.c.joins.job.worker is None).
                sort(this.pk.asc()).
                first().
                insert({
                    'worker': worker,
                    'task': message['task'],
                    'row': this.pk,
                }).
                execute()
            )
            return {
                'type': 'read',
                'data': data,
            }

    def write(self, message):
        job = self.storage.jobs.where(
            this.pipeline == message['pipeline'],
            this.task == message['task'],
            this.worker == worker,
        )

        if message['status'] == 'error':
            job.update({
                'error': message['error'],
            })
        else:
            self.storage(message['pipeline']['name']).insert({
                'data': message['data'],
                'token': message['token']['id'],
                'source_id': message['source']['id'],
                'source_name': message['source']['name'],
            })
            job.delete()

        return {
            'type': 'write',
            'status': 'ok',
        }

    def process_message(self, message):
        try:
            message = schema.execute(message, workers=self.workers)
        except ExecutionError as e:
            errors = e.errors
        else:
            errors = None

        if errors:
            response = {
                'type': 'register',
                'status': 'error',
                'errors': errors,
            }

        # Register new worker to the pool of workers.
        elif message['type'] == 'registration':
            return self.registration(message)

        # Request pipeline for data processing
        elif message['type'] == 'task':
            return self.task(message)

        # Request data for processing.
        elif message['type'] == 'read':
            return self.read(message)

        # Save processed data.
        elif message['type'] == 'write':
            return self.write(message)

        else:
            raise Exception(f"Unhandled message type: {message['type']}.")


    async def __call__(self, websocket, path):
        async for message in websocket:
            message = msgpack.loads(message)
            response = self.process_message(message)
            await websocket.send(msgpack.dumps(response))


def main(argv=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('pipeline', help="path to the pipeline file to run")
    args = parser.parse_args(argv)

    storage = get_storage()

    master = Master(storage, args.pipeline)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(websockets.serve(master, '0.0.0.0', 8765))
    loop.run_forever()
