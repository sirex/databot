import itertools
from typing import List
from databot.expressions import Expression, Handler


class Column:

    def __init__(self, table, name, type, null=True, default=None, pk=False, sequence=None):
        self.table = table
        self.name = name
        self.type = type
        self.null = null
        self.default = default
        self.pk = pk
        self.sequence = sequence

        if self.pk:
            if self.null:
                raise StorageError(f"primary key {self.name!r} cannot be null")
            if self.sequence is None:
                self.sequence = itertools.count(1)
        else:
            if self.sequence is not None:
                raise StorageError(f"non primary key field {self.name!r} cannot have sequence")


class Table:

    def __init__(self, name, columns):
        self.name = name
        self.columns = {c.name: c for c in columns}
        self.sequence = itertools.count(1)
        self.data = {}

    def check(self, data):
        for name, value in data.items():
            if name not in self.columns:
                raise StorageError(f"column {self.name}.{name} does not exist")
            column = self.columns[name]
            if not isinstance(value, column.type):
                expected = column.type.__name__
                got = type(value).__name__
                raise StorageError(f"invalid {self.name}.{name} value type, expentect {expected}, got {got}")

        for column in self.columns.values():
            if column.null is False and column.name not in data or data[column.name] is None:
                raise(f"column {self.name}.{column.name} cannot be null")

    def pk(self, data):
        pk = ()
        for column in self.columns.values():
            if column.pk:
                if column.name not in data:
                    data[column.name] = next(column.sequence)
                pk += (data[column.name],)

        if len(pk) == 1:
            return pk[0]
        elif pk:
            return pk
        else:
            return None

    def insert(self, rows):
        rows = rows if isinstance(rows, list) else [rows]
        if self.name not in self.columns:
            raise StorageError(f"table {self.name} does not exist")

        for data in rows:
            self.check(data)

            for column in self.columns.values():
                if column.default and column.name not in data:
                    data[column.name] = column.default

            pk = self.pk(data) or (next(self.sequence),)

            if pk in self.data:
                key = pk[0] if len(pk) == 1 else ('(' + ', '.join(map(str, pk)) + ')'),
                raise StorageError(f"duplicate key value violates unique constraint for {self.name}, "
                                   f"{key} already exists")

            self.data[pk] = data

    def update(self, data, condition):
        for row in self.select(condition):
            data = row.update(data)
            self.check(data)
            self.data[self.pk(row)] = data

    def select(self, query):
        for


class Query:

    def select(self):
        pass

    def get(self):
        pass


class StorageError(Exception):
    pass


class Storage:

    def __init__(self):
        self._schema = {}
        self._data = {}

    def define(self, name: str, columns: List[Column]):
        self._schema[name] = Table(name, columns)

    def execute(self, expression):
        pass


def get_storage():
    ...


def storage(context) -> Storage:
    return context.storage


def define(this: Storage, *args, **kwargs):
    this.define(*args, **kwargs)


def column(*args, **kwargs) -> Column:
    return Column(*args, **kwargs)


def insert(executor, this: Storage, table: str, rows):
    this[table].insert(rows)


handlers = [
    Handler(object, storage),
    Handler(Storage, define),
    Handler(Table, column),
    Handler(Storage, insert),
    Handler(Query, 'insert'),
    Handler(Query, 'update'),
    Handler(Query, 'delete'),
    Handler(Query, 'select'),
    Handler(Query, 'get'),
    Handler(Query, 'first'),
    Handler(Query, 'where'),
    Handler(Query, 'sort'),
]


storage = Expression('storage')
define = Expression('define')
column = Expression('column')
insert = Expression('insert')
