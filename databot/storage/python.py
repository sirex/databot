from databot.expressions import Handler
from databot.storage import Storage


class PythonStorage(Storage):

    def __init__(self):
        self.tables = {}


def storage(executor, value):
    with executor.context(this=value):
        return executor.step(Storage())


def _getattr_(value, key):
    pass


handlers = [
    Handler(object, storage),
    Handler(Storage, _getattr_, '__getattr__'),
]
