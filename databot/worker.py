import argparse
import asyncio
import websockets


async def worker(master):
    async with websockets.connect(master) as websocket:
        await websocket.send("Hello world!")


def main(argv=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('master', help="master address")
    parser.add_argument('-t', '--task', help="process only specified tasks")
    args = parser.parse_args(argv)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(worker(args.master))
