from setuptools import setup, find_packages


setup(
    name='databot',
    version='0.1',
    packages=find_packages(),
    license='AGPL',
    url='https://gitlab.com/sirex/databot',
    project_urls={
        'Bug Tracker': 'https://gitlab.com/sirex/databot/issues',
        'Source Code': 'https://gitlab.com/sirex/databot',
    }
)
