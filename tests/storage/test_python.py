import pytest

from databot.expressions import Registry, Executor
from databot.handlers import handlers, this
from databot.storage import Storage, write, read, handlers as storage_handlers


@pytest.fixture(scope='module')
def execute():
    registry = Registry()
    registry.add(handlers)
    registry.add(storage_handlers)

    storage = Storage()

    def execute(expression, **context):
        return Executor(registry, **context).execute(expression, storage)

    return execute


def test_storage(execute):
    execute(
        write('test', [
            {'foo': "a", 'bar': 1},
            {'foo': "b", 'bar': 2},
            {'foo': "c", 'bar': 3},
        ])
    )
    assert execute(read('test').select(this.bar)) == [1, 2, 3]
    assert execute(read('test').where(this.bar > 2).get(this.bar)) == 3
    assert execute(
        read('test').
        where(this.foo == 'b').
        update({'bar': 42}).
        select(this.bar)
    ) == [1, 42, 3]
