import re

import pytest

from databot.expressions import Registry, Executor, ExecutionError, NA
from databot.handlers import this, when, each, sub, handlers


@pytest.fixture(scope='module')
def execute():
    registry = Registry()
    registry.add(handlers)

    def execute(expression, this, **context):
        return Executor(registry, **context).execute(expression, this)

    return execute


def test_cast(execute):
    assert execute(this.cast(int), '42') == 42


def test_define(execute):
    assert execute(this.define(a=42).c.a, 1) == 42


def test_call(execute):
    def add(a, b):
        return a + b
    assert execute(this.call(add, 2, 2), 2) == 4
    assert execute(this.call(add, 2, this), 2) == 4
    assert execute(this.call(add, this, 2), 2) == 4
    assert execute(this.call(add, this, this), 2) == 4


def test_apply(execute):
    def add(a, b):
        return a + b
    assert execute(this.apply(add, 2), 2) == 4
    assert execute(this.apply(add, this), 2) == 4
    assert execute(this.apply(add, this**2), 2) == 6


def test_when(execute):
    assert execute(when(this == 42, 'ok'), 42) == 'ok'
    assert execute(when(this == 42, 'ok'), 2) == NA


def test_eq(execute):
    assert execute((this == 42), 42) is True
    assert execute((this == 42), 24) is False


def test_pow(execute):
    assert execute(this**8, 2) == 256


def test_dict_getattr(execute):
    assert execute(this.a.b.c, {'a': {'b': {'c': 42}}}) == 42


def test_check(execute):
    expression = this.check(this > 42, "error: {value}", {'value': this})

    assert execute(expression, 64) == 64

    with pytest.raises(ExecutionError) as e:
        execute(expression, 1)
    assert e.value.result == 1
    assert str(e.value).splitlines() == [
        "Expression:",
        "  databot.handlers._expression_ | this.check",
        "  databot.handlers.this | 1",
        "  databot.handlers.check | 1",
        "Error: error: 1",
    ]


def test_exception(execute):
    def func():
        raise Exception('ERR')

    expression = this.call(func)

    with pytest.raises(ExecutionError) as e:
        execute(expression, None)

    clean = this.cast(str).splitlines().each(
        sub(r'File "[^"]+"', 'File "..."').
        sub(r'line \d+,', 'line #,').
        sub(r'at 0x[0-9a-f]+', 'at 0x#')
    )
    assert execute(clean, e.value) == [
        'Expression:',
        '  databot.handlers._expression_ | this.call',
        '  databot.handlers.this | None',
        '  databot.handlers.call | None',
        'Traceback:',
        '  File "...", line #, in execute',
        '    this = handler(self, op, expr, this)',
        '  File "...", line #, in __call__',
        '    this = self.func(*args, **kwargs)',
        '  File "...", line #, in call',
        '    return func(*args, **kwargs)',
        '  File "...", line #, in func',
        "    raise Exception('ERR')",
        'Error: ERR',
    ]


def test_each(execute):
    assert execute(each(this**2), [1, 2, 3]) == [1, 4, 9]
    assert execute(each(this**2), (1, 2, 3)) == (1, 4, 9)
    assert execute(each(this**2), {1, 2, 3}) == {1, 4, 9}
    assert execute(each(this**2), {'a': 1, 'b': 2, 'c': 3}) == {'a': 1, 'b': 4, 'c': 9}


def test_sub(execute):
    assert execute(this.sub(r'\d+', 'd'), '1 2 3') == 'd d d'
