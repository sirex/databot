import argparse
import datetime
import pathlib
import uuid
import sys

from peewee import (
    Model,
    Proxy,
    CharField,
    IntegerField,
    BigIntegerField,
    BigAutoField,
    FixedCharField,
    TextField,
    TimestampField,
    ForeignKeyField,
    BooleanField,
    UUIDField,
    SmallIntegerField,
    BlobField,
)
from playhouse.postgres_ext import (
    PostgresqlExtDatabase,
    BinaryJSONField,
)

from databot.expressions import Executor
from databot.expressions import Registry
from databot.expressions import stack

from databot import handlers

UNIQUE = True

db = Proxy()


class BaseModel(Model):

    class Meta:
        database = db


class Bot(BaseModel):
    bot = CharField(primary_key=True)
    code = TextField(null=False)
    version = IntegerField(null=False)
    created = TimestampField(utc=True, default=datetime.datetime.utcnow())
    updated = TimestampField(utc=True)


class Task(BaseModel):
    bot = ForeignKeyField(Bot, null=False)
    name = CharField(null=False)

    class Meta:
        indexes = (
            (('bot', 'name'), UNIQUE),
        )


class Pipe(BaseModel):
    bot = ForeignKeyField(Bot, null=False)
    name = CharField()
    created = TimestampField(utc=True, default=datetime.datetime.utcnow())

    class Meta:
        indexes = (
            (('bot', 'name'), UNIQUE),
        )


class Worker(BaseModel):
    id = UUIDField(primary_key=True, default=uuid.uuid4)
    active = BooleanField()
    last_time_seen = TimestampField(utc=True)
    created = TimestampField(utc=True, default=datetime.datetime.utcnow())
    name = CharField(null=False)


class State(BaseModel):
    task = ForeignKeyField(Task, null=False)
    pipe = ForeignKeyField(Pipe, null=False)
    offset = BigIntegerField(null=False)


class Job(BaseModel):
    created = TimestampField(utc=True, default=datetime.datetime.utcnow())
    worker = ForeignKeyField(Worker, null=False)
    task = ForeignKeyField(Task, null=False)
    pipe = ForeignKeyField(Pipe)
    row = BigIntegerField()


class Migration(BaseModel):
    name = CharField()
    created = TimestampField(utc=True, default=datetime.datetime.utcnow())
    applied = TimestampField(utf=True, null=True)


def define_data_table(pipe: Pipe):

    class Data(BaseModel):
        id = BigAutoField()
        key = FixedCharField(160, null=False)  # sha1
        data = BinaryJSONField(null=False)
        datac = BlobField(null=True)  # compressed data
        compressed = SmallIntegerField(null=False, default=0)  # compression algorithm
        created = TimestampField(utc=True, default=datetime.datetime.utcnow())
        bot = ForeignKeyField(Bot, null=False)
        task = ForeignKeyField(Task, null=False)
        worker = ForeignKeyField(Worker, null=False)
        source = ForeignKeyField(Pipe)
        source_id = BigIntegerField()  # primary key of source Pipe
        errors = BinaryJSONField()
        retries = IntegerField(null=False, default=0)

        class Meta:
            table_name = f'D{pipe.id:04d}'

    return Data


class Error(Exception):
    pass


class Task:

    def __init__(self, ns, name, expr):
        self.ns = ns
        self.name = name
        self.expr = expr

    def get_next_row(self):
        pass


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('path', type=pathlib.Path)
    args = parser.parse_args()

    if not args.path.exists():
        raise Error("Error: path %s does not exists.")

    db.initialize(PostgresqlExtDatabase('databot'))
    db.connect()
    db.create_tables([
        State,
    ])

    registry = Registry()
    registry.add(handlers)

    executor = Executor(registry)

    for path in args.path.rglob('*.py'):
        globalvars = {
            '__file__': str(path),
            '__name__': '__main__',
        }
        localvars = {}
        exec(compile(path.read_text(), str(path), 'exec'), globalvars, localvars)

        ns = str(path.relative_to(args.path)).replace('/', '.')[:-3]

        for task in localvars['pipeline']['tasks']:
            op = next(stack(task), None)
            if op is None:
                raise Error("Empty expression.")
            if op.name != 'task':
                raise Error(
                    f"All task expressions have start with a task('nam') call, but this was started with {op.name!r}."
                )
            nargs = len(op.args)
            if nargs != 1:
                raise Error(f"Task funchtion expects exactly one argument, task name, but {nargs} got.")
            name = op.args[0]

            print(ns, name)

            executor.execute(task, Task(ns, name, task))


if __name__ == "__main__":
    sys.exit(main() or 0)
